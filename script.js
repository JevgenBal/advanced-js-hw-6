const findIPButton = document.getElementById('findIPButton');
const resultDiv = document.getElementById('result');
const loaderDiv = document.getElementById('loader');

findIPButton.addEventListener('click', async () => {
    try {
        showLoader();
        const ipAddress = await getIpAddress();
        const locationInfo = await getLocationInfo(ipAddress);

        displayLocationInfo(locationInfo);
        hideLoader();
    } catch (error) {
        console.error(error);
        hideLoader();
    }
});

function showLoader() {
    loaderDiv.style.display = 'inline-block';
}

function hideLoader() {
    loaderDiv.style.display = 'none';
}

async function getIpAddress() {
    const response = await fetch('https://api.ipify.org/?format=json');
    const data = await response.json();
    return data.ip;
}

async function getLocationInfo(ipAddress) {
    const response = await fetch(`http://ip-api.com/json/${ipAddress}?fields=status,continent,country,regionName,city,district`);
    const data = await response.json();
    return data;
}

function displayLocationInfo(locationInfo) {
    resultDiv.innerHTML = `
        <p><strong>Континент:</strong> ${locationInfo.continent || 'none'}</p>
        <p><strong>Країна:</strong> ${locationInfo.country || 'none'}</p>
        <p><strong>Регіон:</strong> ${locationInfo.regionName || 'none'}</p>
        <p><strong>Місто:</strong> ${locationInfo.city || 'none'}</p>
        <p><strong>Район:</strong> ${locationInfo.district || 'none'}</p>
    `;
}